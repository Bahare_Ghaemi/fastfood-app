export default [{
        title: 'مگاچیز برگر',
        price: 110000,
        pricePer: '۱۱۰,۰۰۰',
        description: "دو عدد برگر گوشت سفارشی ، دو عدد پنیر ورقه ای گودا ، سس مخصوص ، گوجه،کاهو و خیار شور در فست فود کندل میتونه به اندازه ای سیرتون کنه که اون لحظه فک کنید هیج وقت دیگه گرسنه نمیشوید. شما میتوانید هر لحظه این مگاچیز برگر خوشمزه رو در فست فود کندل میل کنید.",
        category: 'همبرگر',
        image: require('../assets/images/مگا چیز برگر.png')
    },
    {
        title: 'پیتزا پپرونی',
        price: 120000,
        pricePer: '۱۲۰,۰۰۰',
        description: "پیتزا پپرونی یکی از انواع پیتزاهای معروف و بسیار پرطرفداردر جهان است این پیتزا با یک نوع سوسیس مخصوص تهیه میشود که پپرونی نام دارد و به همین دلیل با نام پیتزا پپرونی شناخته میشود که غذای محبوب در میان دوستداران غذاهای تند و ادویه دار در فست فود کندل است. پیتزا پپرونی از کالری بالایی برخوردار است چون در این غذا چیزی جز پنیر و پپرون یافت نمیشود ولی با این حال گذشتن از این پیتزای خوشمزه کار خیلی دشواریست.",
        category: 'پیتزا',
        image: require('../assets/images/P_Pepperoni-removebg-preview.png')
    },
    {
        title: 'ساندویچ ژامبون سرد',
        price: 99000,
        pricePer: '۹۹,۰۰۰',
        description: "200 گرم ژامبون گوشت و مرغ 90% و یه سس خاص",
        category: 'ساندویچ',
        image: require('../assets/images/389f3043.png')
    },
    {
        title: 'پیتزا رست بیف (دونفره)',
        price: 145000,
        pricePer: '۱۴۵,۰۰۰',
        description: "200 گرم ژامبون گوشت و مرغ 90% و یه سس خاص",
        category: 'پیتزا',
        image: require('../assets/images/P_RoastBeef-removebg-preview.png')
    },
    {
        title: 'ساندویچ هات داگ کلاسیک',
        price: 90000,
        pricePer: '۹۰,۰۰۰',
        description: "یه هات داگ تنوری حرفه ای با سس خردل",
        category: 'ساندویچ',
        image: require('../assets/images/classic-hotdog.png')
    },
    {
        title: 'گریل چیکن',
        price: 75000,
        pricePer: '۷۵,۰۰۰',
        description: "گریل چیکن های فست فود کندل به شدت خوشمزه و دلچسب است . این برگر دوست داشتنی را به طرفداران غذاهای رژیمی توصیه میکنیم ، یک تکه سینه مرغ گریل شده به همرا سبزیجات تازه میتواند بهترین انتخاب باشد البته اگر رژیم دارید بدون پنیر وسس ان را سفارش دهید",
        category: 'همبرگر',
        image: require('../assets/images/grill-chicken.png')
    },
    {
        title: 'پیتزا سبزیجات',
        price: 105000,
        pricePer: '۱۰۵,۰۰۰',
        description: "پیتزا سبزیجات خوشمزه یک فست فود سالم که میتواند انتخاب مناسبی برای گیاهخوران طرفدار فست فود کندل باشد برای طبخ پیتزای سبزیجات رژیمی، میتوان  از پنیر کمتری استفاده کرد",
        category: 'پیتزا',
        image: require('../assets/images/P_Vegetable.png')
    },
    {
        title: 'ساندویچ ژامبو زینگر',
        price: 126000,
        pricePer: '۱۲۶,۰۰۰',
        description: "ساندویچ فیله مرغ سوخاری شده با 80گرم ژامبون گوشت و قارچ و پنیر و یه سس خاص",
        category: 'ساندویچ',
        image: require('../assets/images/ata-zinger.png')
    },
    {
        title: 'پیتزا میکس',
        price: 130000,
        pricePer: '۱۳۰,۰۰۰',
        description: "پیتزا میکس ترکیبی از فیله مرغ گریل شده و چند ورق استیک گوشت است و البته فلفل هالوپینو که طعم تند دلنشینی به این پیتزا خوشمزه میدهد قابل ذکر است که در صورت نیاز میتوان فلفل هالوپینو راحذف کرد این پیتزا با رسپی های  متفاوتی پخته میشود اما پیتزا میکس فست فود کندل یکی از بهترین هاست کافیست یکبار مزه ان را چشیده باشین تا دلیل ان را درک کنید",
        category: 'پیتزا',
        image: require('../assets/images/P_Mix-removebg-preview.png')
    },
    {
        title: 'سوپریم',
        price: 95000,
        pricePer: '۹۵,۰۰۰',
        description: "سوپریم جزو پرفروش ترینهای فست فود کندل است ، برگری که تشکیل شده از فیله سوخاری شده با طعمی بینظیر، یک ورق کالباس سرخ شده، پنیر گودا ،سسی که مخصوص این برگر در فست فود کندل درست شده و سبزیحات تازه و رنگارنگ، مطمئنا با فکر کرن به جزئیات این برگر نمیتوانید از سفارش آن به راحتی چشم پوشی کنید",
        category: 'همبرگر',
        image: require('../assets/images/سوپریم-برگر-76.png')
    },
    {
        title: 'ماشروم برگر',
        price: 80000,
        pricePer: '۸۰,۰۰۰',
        description: "طعم واقعی ماشروم را در فست فود کندل امتحان کنید میتوان با جرات گفت که سس ماشروم ما از منحصر بفردترین هاست یک برگر گریل شده به همراه سبزیجات، یک ورق پنیر گودا و سس ماشروم میتواند یک وعده غذای کامل و سالم را برای شما ورق بزند",
        category: 'همبرگر',
        image: require('../assets/images/mushroom-burger.png')
    },
]